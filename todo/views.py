from django.contrib.auth.decorators import login_required
from todo.models import ToDoList
from django.views.generic import TemplateView


class TodoListsListView(TemplateView):
    template_name = 'user_todo_lists_listview.html'

    def get_context_data(self, **kwargs):
        context = super(TodoListsListView, self).get_context_data()
        user = self.request.user
        user_lists = ToDoList.objects.filter(owner=user)
        context.update({'user': user, 'user_lists': user_lists})
        return context


class TodoListsView(TemplateView):
    template_name = 'user_todo_list_view.html'

    def get_context_data(self, **kwargs):
        context = super(TodoListsView, self).get_context_data()
        user_list = ToDoList.objects.get(id=kwargs.get('list_id'), owner=self.request.user)
        context.update({'list': user_list})
        return context


user_todolists_listview = login_required(TodoListsListView.as_view())
user_todolist_view = login_required(TodoListsView.as_view())
