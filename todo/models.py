from django.db import models
from django.contrib.auth.models import User


class ToDo(models.Model):
    list = models.ForeignKey('todo.ToDoList')
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=300)
    created_datetime = models.DateTimeField(auto_now=True)
    due_datetime = models.DateTimeField(auto_now=True)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return '{} ({})'.format(self.title, self.list.title)


class ToDoList(models.Model):
    title = models.CharField(max_length=200)
    owner = models.ForeignKey(User)

    def __str__(self):
        return '{} ({})'.format(self.title.upper(), self.owner)
