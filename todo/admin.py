from django.contrib import admin
from todo.models import ToDoList, ToDo


class ToDoInlineAdmin(admin.StackedInline):
    model = ToDo


class ToDoAdmin(admin.ModelAdmin):
    pass


class ToDoListAdmin(admin.ModelAdmin):
    inlines = (ToDoInlineAdmin,)


admin.site.register(ToDo, ToDoAdmin)
admin.site.register(ToDoList, ToDoListAdmin)
