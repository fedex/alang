from django.conf.urls import url
from .views import user_todolists_listview, user_todolist_view

urlpatterns = [
    url(r'^todo_list/(?P<list_id>[0-9]+)/$', user_todolist_view, name='user_todolist_view'),
    url(r'', user_todolists_listview),
]

